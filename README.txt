A very early Iphone module.

Currently the module displays articles from Observer.com. It is just a demo of Iphone in Drupal. The module will eventually become more generic.

Requirements
-------
Download the IUI framework into this package - http://code.google.com/p/iui

Credits
------------
Drupal module by Moshe Weitzman <weitzman AT tejasa DOT com>
IUI web framework by Joe Hewitt - http://joehewitt.com